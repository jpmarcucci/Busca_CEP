# -*- coding: utf-8 -*-

import requests
import sys
from bs4 import BeautifulSoup

URL="http://www.buscacep.correios.com.br/sistemas/buscacep/resultadoBuscaCepEndereco.cfm"
CEP="13503532"

if (len(CEP) != 8) or not (CEP.isdigit()):
	print("O CEP tem que ter 8 digitos e conter apenas numeros, exemplo: 13503532")
	sys.exit()

try:
	response = requests.post(URL, data={
		"tipoCEP": "ALL",
		"semelhante": "N",
		"relaxation": CEP,
		}, timeout=10, headers={"Accept-Encoding": "identity"})
except:
	print("Erro ao buscar informacoes no site dos Correios")
	sys.exit()

if ((response.text).find("DADOS NAO ENCONTRADOS")) > 0:
	print("CEP nao encontrado na base de dados dos correios")
	sys.exit()

soup = BeautifulSoup(response.text, "lxml")
table = soup.find("table", attrs={"class":"tmptabela"})

titulo = [th.get_text() for th in table.find("tr").find_all("th")]
for row in table.find_all("tr")[1:]:
	r=(zip(titulo, (td.get_text() for td in row.find_all("td"))))

x=(r[2][1]).split("/")

r_ENDERECO=(r[0][1]).upper()
r_BAIRRO=(r[1][1]).upper()
r_CIDADE=(x[0]).upper()
r_ESTADO=(x[1]).upper()
r_CEP=(r[3][1])

print("Endereco: " + r_ENDERECO)
print("Bairro: " + r_BAIRRO)
print("Cidade: " + r_CIDADE)
print("Estado: " + r_ESTADO)
print("CEP: " + r_CEP)
