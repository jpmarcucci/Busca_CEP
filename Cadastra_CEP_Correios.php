<?php
setlocale(LC_CTYPE, 'pt_BR');

$URL="http://www.buscacep.correios.com.br/sistemas/buscacep/resultadoBuscaCepEndereco.cfm";
$CEP = $_GET["cep"];

function BuscaCEP($CEP){
        global $URL;
        $dados = array('relaxation' => $CEP, 'semelhante' => 'N', 'tipoCEP' => 'ALL');

        $requisicao = array(
                'http' => array(
                'header' => "Content-Type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($dados),
                'timeout' => 5
                )
        );

        $contexto=stream_context_create($requisicao);

        $resultado = @file_get_contents($URL, false, $contexto);

        if (!($resultado)){
                echo ("Falha ao buscar informacoes no site dos Correios.");
                exit();
        }

        if(strpos($resultado,"DADOS NAO ENCONTRADOS")){
                echo("CEP nao encontrado na base de dados dos Correios.");
                exit();
        }

        $doc = new DOMDocument();
        @$doc->loadHTML($resultado);
        $x = new DOMXpath($doc);

        $r_ENDERECO=$x->query('//td')->item(0)->textContent;
        $r_BAIRRO=$x->query('//td')->item(1)->textContent;
        $r_CEP=str_replace("-","",$x->query('//td')->item(3)->textContent);
        $c=$x->query('//td')->item(2)->textContent;
        $c = explode("/", $c);
        $r_CIDADE=$c[0];
        $r_ESTADO=strtoupper($c[1]);

        //Retira os acentos
        $r_BAIRRO = strtoupper(preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $r_BAIRRO ) ));
        $r_ENDERECO = strtoupper(preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $r_ENDERECO ) ));
        $r_CIDADE = strtoupper(preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $r_CIDADE ) ));
        $r_ESTADO = strtoupper(preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $r_ESTADO ) ));

        echo "Endereco: " . $r_ENDERECO . "\n";
        echo "Bairro: " . $r_BAIRRO . "\n";
        echo "Cidade: " . $r_CIDADE . "\n";
        echo "Estado: " . $r_ESTADO . "\n";
        echo "CEP: " . $r_CEP . "\n";
}

function VerificaCEP($i){
        if((strlen($i) != 8) or !(is_numeric($i))){
                echo("O CEP tem que ter 8 digitos e conter apenas numeros");
        }else{
                BuscaCEP($i);
        }
}

VerificaCEP($CEP);
?>
