# -*- coding: utf-8 -*-

import json
import requests
import sys
from unicodedata import normalize

def remove_acentos(texto, codificacao='utf-8'):
        return normalize('NFKD', texto).encode('ASCII', 'ignore')

try:
        URL_API="http://api.postmon.com.br/v1/cep/"
        CEP = sys.argv[1]
        URL = URL_API + str(CEP)
except:
        print("Uso: python " + sys.argv[0] + " [CEP]")
        print("Exemplo: python " + sys.argv[0] + " 13502560")
        sys.exit()

try:
        RESULTADO = requests.get(URL)
        j = json.loads(RESULTADO.text)
except:
        print("Erro ao retornar informacoes do CEP.")
        sys.exit()

CEP=j['cep']
CIDADE=j['cidade'].upper()
UF=j['estado'].upper()
ENDERECO=j['logradouro'].upper()
BAIRRO=j['bairro'].upper()

print("Endereco de {}: {}, {} - {}/{}".format(CEP, ENDERECO, BAIRRO, CIDADE, UF));
