<?php

$URL_API="http://api.postmon.com.br/v1/cep/";
//setlocale(LC_CTYPE, 'pt_BR');

function BuscaCEP(){
	global $argv;
        global $URL_API;

        $CEP = @$argv[1];

        if($CEP==''){
		echo "CEP nao fornecido";
                exit;
        }

	$json = @file_get_contents($URL_API.$CEP);

        if($json == true){
                $a = json_decode($json);

                $r_CEP = $a->cep;
                $r_BAIRRO = $a->bairro;
                $r_ENDERECO = $a->logradouro;
                $r_CIDADE = $a->cidade;
                $r_ESTADO = strtoupper($a->estado);

                //Retira os acentos:
                $r_BAIRRO = strtoupper(preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $r_BAIRRO ) ));
                $r_ENDERECO = strtoupper(preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $r_ENDERECO ) ));
                $r_CIDADE = strtoupper(preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $r_CIDADE ) ));


                echo "Endereco de $r_CEP: $r_ENDERECO - $r_BAIRRO. $r_CIDADE/$r_ESTADO.";
        }else{
                echo "Falha ao buscar CEP: $CEP";
        }
}

BuscaCEP();

?>
